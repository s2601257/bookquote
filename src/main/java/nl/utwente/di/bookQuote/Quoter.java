package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public Quoter() {
    }
    public double getBookPrice(String isbn) {
        HashMap<String,Double> bookQuote = new HashMap<>();
        bookQuote.put("1", 10.0);
        bookQuote.put("2", 45.0);
        bookQuote.put("3", 20.0);
        bookQuote.put("4", 35.0);
        bookQuote.put("5", 50.0);
        if(!bookQuote.containsKey(isbn)){
            return 0.0;
        }
        return bookQuote.get(isbn);
    }
}
